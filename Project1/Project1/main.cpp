#include <SFML/Window.hpp>
#include <iostream>
#include <SFML/Graphics.hpp>
#include <SFML/Audio.hpp>
#include <cmath>
#include <vector>
#include <fstream>
#include <string>
#include <array>

using namespace std;

//Function for generating text
void importText(string text, sf::Font font, int size, vector<sf::Text>& targetList)
{
	targetList.push_back(sf::Text(text, font, size));
}

//Function for importing sound files
void importSound(string dir, sf::SoundBuffer& buffer, sf::Sound& sound, double volume)
{
	buffer.loadFromFile(dir);
	sound.setBuffer(buffer);
	sound.setVolume(volume);
}

//Function for centering text in container
void singleTextResizing(sf::Text& text, int startingX, int startingY)
{
	sf::FloatRect textRect;
	textRect = text.getLocalBounds();
	text.setOrigin(textRect.left + textRect.width / 2.0f,
		textRect.top + textRect.height / 2.0f);
	text.setPosition(startingX, startingY);
}

//Function for centering text in list
void textResizing(vector<sf::Text>& inputVector, int startingX, int startingY,
	int deltaY)
{
	for (unsigned int i(0); i < inputVector.size();i++)
	{
		singleTextResizing(inputVector[i], startingX, startingY);
		inputVector[i].setPosition(startingX, startingY + i * deltaY);
		inputVector[i].setFillColor(sf::Color::White);
	}
}

class titleScreen {
public:

	void importText(string text, sf::Font font, int size)
	{
		titleText.push_back(sf::Text(text, font, size));
	}
	void background(sf::RenderWindow& window)
	{
		window.clear(sf::Color::White);
		for (int i(0); i < titleText.size();i++)
			window.draw(titleText[i]);
		window.display();
	}

	vector<sf::Text> titleText;

};

class difficultyScreen {
public:
	vector<sf::Text> difficultyText;

};

class musicScreen {
public:
	vector<sf::Text> musicText;
	vector<sf::SoundBuffer> musicBuffer;
};

class settingScreen {
public:
	vector<vector<sf::Text>> settingText;
	int musicVolume;
};

class gamescreen {
public:

};

//Finished
class introScreen {
public:
	sf::Texture introElements[4];
	sf::Sprite introSprite;
	sf::Sprite transition;
	sf::Event skip;
	sf::SoundBuffer BGM;
	sf::Sound menuMusic;

	void importTexture(string dir, int i)
	{
		introElements[i].loadFromFile(dir);
	}

	void intro(sf::RenderWindow& window)
	{
		menuMusic.setPlayingOffset(sf::seconds(56.f));
		menuMusic.setLoop(true);
		int cycle(0);
		for (int i(0); i < 3; i++)
		{
			introSprite.setTexture(introElements[i + 1]);
			while (cycle <= 360)
			{
				if (i == 1 && cycle < 48 && cycle % 4 == 0)
					menuMusic.setVolume(cycle / 4);
				if (cycle <= 60)
					transition.setColor(sf::Color(255, 255, 255, -4 * cycle + 240));
				else if (cycle >= 300 && cycle <= 360)
					transition.setColor(sf::Color(255, 255, 255, 4 * (cycle - 300)));
				window.clear(sf::Color::Black);
				window.draw(introSprite);
				window.draw(transition);
				window.display();

				while (window.pollEvent(skip))
				{
					if (skip.type == sf::Event::KeyPressed)
					{
						if (skip.key.code == sf::Keyboard::Enter || skip.key.code == sf::Keyboard::Escape || skip.key.code == sf::Keyboard::Space)
						{
							cycle = 360;
							menuMusic.setVolume(11);
							break;
						}
					}
				}
				cycle++;
			}
			if (i == 0)
				menuMusic.play();
			cycle = 0;
		}
	}
};

int main()
{
	introScreen intro;
	titleScreen title;
	difficultyScreen difficulty;
	musicScreen music;
	settingScreen settings;
	gamescreen game;

	bool menuscreen(true), difficultyscreen(false), musicscreen(false),
		settingscreen(false), gamescreen(false);
	settings.musicVolume = 5;
	//|||||||||||||||||||||||||||||||||||||||||||||Load files
	//Load Textures
	intro.importTexture("Textures/black.png",0);
	intro.importTexture("Textures/disclaimer.png",1);
	intro.importTexture("Textures/studioTitle.png",2);
	intro.importTexture("Textures/sfml.png",3);
	intro.transition.setTexture(intro.introElements[0]);
	
	//Load font
	sf::Font invasion, Futura;
	invasion.loadFromFile("Miscs/Invasion.otf");
	Futura.loadFromFile("Miscs/Futura.ttf");
	
	//Load main menu text
	/*
	importText("Start", invasion, 30, title.titleText);
	importText("Difficulty", invasion, 30, title.titleText);
	importText("BGM", invasion, 30,title.titleText);
	importText("Leaderboard", invasion, 30, title.titleText);
	importText("Settings", invasion, 30, title.titleText);
	importText("Quit", invasion, 30, title.titleText);
	importText("  SPACE\nBREAKOUT", invasion, 90, title.titleText);
	importText("Created by Jinhan Liu\nVersion Number a1.00", Futura, 18, title.titleText);
	*/
	title.importText("Start", invasion, 30);
	title.importText("Difficulty", invasion, 30);
	title.importText("BGM", invasion, 30);
	title.importText("Leaderboard", invasion, 30);
	title.importText("Settings", invasion, 30);
	title.importText("Quit", invasion, 30);
	title.importText("  SPACE\nBREAKOUT", invasion, 90);
	title.importText("Created by Jinhan Liu\nVersion Number a1.00", Futura, 18);


	//Load difficulty text
	importText("Medium", invasion, 30, difficulty.difficultyText);
	importText("Hard", invasion, 30, difficulty.difficultyText);
	importText("Impossible", invasion, 30, difficulty.difficultyText);
	//Load music text
	importText("Stardust", invasion, 30, music.musicText);
	importText("Spaceflight", invasion, 30, music.musicText);
	importText("Lost Future", invasion, 30, music.musicText);
	importText("Press Space to start or stop sampling.", invasion, 20, music.musicText);
	//Load settings text

	//Load game text

	//Load Music
	importSound("Musics/menuMusic.ogg", intro.BGM, intro.menuMusic, 40);
	

	//Load sound effects


	sf::RenderWindow window(sf::VideoMode(800, 600), "Space Breakout");
	window.setVerticalSyncEnabled(true);
	window.setFramerateLimit(60);

	//Intro Screen
	intro.intro(window);
	
	while (window.isOpen())
	{
		//Reset menu item position
		textResizing(title.titleText, 400, 320, 40);
		title.titleText[6].setPosition(400, 180);
		title.titleText[6].setFillColor(sf::Color::Red);
		title.titleText[7].setPosition(90, 570);

		while (menuscreen)
		{
			title.background(window);

		}
		while (difficultyscreen)
		{

		}
		while (musicscreen)
		{

		}
		while (settingscreen)
		{

		}
		while (gamescreen)
		{

		}
		
	}

	return 0;
}